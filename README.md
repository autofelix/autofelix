<img align="right" style="pointer-events:none;" src="https://gitlab-readme-stats.vercel.app/api?username=autofelix&show_icons=true&icon_color=E65A65&text_color=adbac7&bg_color=2d333b&hide_title=true&hide_border=true" />

### Hello World! I am <b><a target="_blank" href="javascript:;">autofelix</a></b>.

- :hearts: Passionate about open source software. 
- :hearts: My projects are trusted by developers.
- :sun_with_face: What i have achieved.

<a href="https://autofelix.blog.csdn.net">
    <img src="https://img.shields.io/badge/CSDN PV-115K-E65A65.svg" alt="" title="autofelix的csdn" />
</a>

<a href="https://cloud.tencent.com/developer/user/8345747/articles">
    <img src="https://img.shields.io/badge/Tencentcloud PV-200K-blue.svg" alt="" title="autofelix的腾讯云社区" />
</a>

---

| repository | homepage | stars | last commit | NPM downloads | top language
| --- | --- | :--- | :--- | :--- | ---: |
| [vue-element-admin](https://github.com/PanJiaChen/vue-element-admin) <img src="https://autofelix.github.io/autofelix/assets/icons/vue.svg" height="14px" /> 🔥 | [`👀 preview`](https://panjiachen.github.io/vue-element-admin) | [![GitHub stars](https://img.shields.io/github/stars/PanJiaChen/vue-element-admin?style=flat-square&label=✨)](https://github.com/PanJiaChen/vue-element-admin/stargazers) | [![GitHub last commit](https://img.shields.io/github/last-commit/PanJiaChen/vue-element-admin?style=flat-square&label=%20)](https://github.com/PanJiaChen/vue-element-admin/commits) | [![NPM downloads](https://img.shields.io/npm/dy/vue-element-admin?style=flat-square&label=⚡&color=cb3837&labelColor=231f20)](https://www.npmjs.com/package/vue-element-admin) | ![GitHub top language](https://img.shields.io/github/languages/top/PanJiaChen/vue-element-admin?style=flat-square)
| [vue-admin-template](https://github.com/PanJiaChen/vue-admin-template) <img src="https://autofelix.github.io/autofelix/assets/icons/vue.svg" height="14px" /> | [`👀 preview`](https://panjiachen.github.io/vue-admin-template) | [![GitHub stars](https://img.shields.io/github/stars/PanJiaChen/vue-admin-template?style=flat-square&label=✨)](https://github.com/PanJiaChen/vue-admin-template/stargazers) | [![GitHub last commit](https://img.shields.io/github/last-commit/PanJiaChen/vue-admin-template?style=flat-square&label=%20)](https://github.com/PanJiaChen/vue-admin-template/commits) | [![NPM downloads](https://img.shields.io/npm/dy/vue-admin-template?style=flat-square&label=⚡&color=cb3837&labelColor=231f20)](https://www.npmjs.com/package/vue-admin-template) | ![GitHub top language](https://img.shields.io/github/languages/top/PanJiaChen/vue-admin-template?style=flat-square)
| [python-spiders 🔥](https://github.com/autofelix/python-spiders) | - | [![GitHub stars](https://img.shields.io/github/stars/autofelix/python-spiders?style=flat-square&label=✨)](https://github.com/autofelix/python-spiders/stargazers) | [![GitHub last commit](https://img.shields.io/github/last-commit/autofelix/python-spiders?style=flat-square&label=%20)](https://github.com/autofelix/python-spiders/commits) | - | ![GitHub top language](https://img.shields.io/github/languages/top/autofelix/python-spiders?style=flat-square)